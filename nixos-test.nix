{ system
, nixpkgs
, ...
}:
let
  inherit (import (nixpkgs + "/nixos/lib/testing-python.nix") { inherit system; }) makeTest;
in
makeTest {
  name = "Homepage test";

  nodes = {
    client = { pkgs, ... }: { environment.systemPackages = with pkgs; [ curl ]; };
    server = import ./configuration.nix;
  };

  testScript =
    { ... }:
    ''
      for machine in client, server:
          machine.wait_for_unit("network.target")

      server.wait_for_unit("nginx.service")
      server.wait_for_open_port(80)

      with subtest("Fetch index.html"):
          client.succeed("curl --connect-timeout 10 --fail http://server/index.html")
    '';
}
