{ ... }:
let
  rootDisk = "/dev/sda";
in
{
  imports = [ ./homepage.nix ];

  boot.loader = {
    grub.device = rootDisk;
  };
  dadada.homePage.enable = true;
  networking.firewall.allowedTCPPorts = [ 80 ];
  fileSystems."/".device = rootDisk + "1";
  services.sshd.enable = true;
  system.stateVersion = "23.05";
}
