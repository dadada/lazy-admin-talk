{
  description = "Lazy Sysadmin Talk";

  inputs.devshell.url = "github:numtide/devshell";
  inputs.flake-utils.url = "github:numtide/flake-utils";

  outputs = { flake-utils, devshell, nixpkgs, ... }:
    let
      system = "x86_64-linux";
    in
    {
      nixosConfigurations.lazy-sysadmin = nixpkgs.lib.nixosSystem {
        inherit system;
        modules = [ (import ./configuration.nix) ];
      };
      nixosModules.homePage = import ./homepage.nix;
      checks.${system}.homePage = import ./nixos-test.nix { inherit nixpkgs system; };
    }

    #######################################################################
    # Conveniece things

    // flake-utils.lib.eachDefaultSystem (system:
      let pkgs = import nixpkgs { inherit system; }; in
      {
        devShells.default =
          let
            pkgs = import nixpkgs {
              inherit system;
              overlays = [ devshell.overlays.default ];
            };
          in
          pkgs.devshell.mkShell {
            packages = with pkgs; [ glow nixpkgs-fmt nil pandoc texlive.combined.scheme-full ];
            commands = [
              {
                name = "slides";
                command = "glow --pager lazy-admin.md";
                help = "Shows the slides";
              }
              {
                name = "config";
                command = "nix build .#nixosConfigurations.lazy-sysadmin.config.system.build.toplevel --rebuild";
                help = "Build the config";
              }
              {
                name = "check";
                command = "nix build .\#checks.x86_64-linux.homePage --print-build-logs --rebuild";
                help = "Check configuration";
              }
              {
                name = "pdf";
                command = "pandoc --pdf-engine lualatex -t beamer lazy-admin.md -o lazy-admin.pdf";
                help = "Build the slides";
              }
            ];
          };
        formatter = pkgs.nixpkgs-fmt;
      });
}
