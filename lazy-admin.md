# NixOS for Lazy Admins

## Problems

- Keeping everything documented is hard work.
- Maintaining deployment and automation scripts is hard work.
- *We don't want to do hard work.*

## Comparison

- etckeeper ☹️
  - Infrastructure -> code
  - Documentation post-facto
- Ansible 😐
  - Code -> infrastructure
  - Generic task automation
  - Stateful (applied once, not always reproducible)
- NixOS ☺️
  - Code -> infrastructure
  - Stateless (applied often, reproducible)

## What is Nix?

- Like JSON, but with comments and *functions*.
- Some functions (`derivation`) can run code to output files.
- Output files are stored in `/nix/store/somelonghash-outputname/somefile`.
- [nixpkgs](https://github.com/NixOS/nixpkgs) is a collection of Nix functions.

## Example: Nix function that returns a NixOS configuration

`./configuration.nix`

## What is NixOS?

- Also part of nixpkgs
- Collection of Nix functions (modules) that generate
  - Software packages (e.g. OpenSSH, systemd, Firefox, ...)
  - Configuration files (e.g. sshd_config, systemd units, Firefox profiles, ...)

## Modules

- A function that
  - takes a set of attributes `{ pkgs, lib, config, ... }` and
  - returns a set of `{ imports, options, config, ... }`.
- Each module can individually inspect and set the value of all other options.
- The configuration is merged during the evaluation.

## Example: NixOS module

`./homepage.nix`

## Flakes

- Makes NixOS configurations discoverable and reproducible.
- `flake.nix`
  - Defines dependencies (e.g. other repos).
  - Lists Nix expressions (e.g. NixOS configurations).
- `flake.lock`
  - Defines exact version of dependencies.

## Example: Flake to tie it all together

`./flake.nix`

## Fearless Upgrades

- Update flake.lock -> Deploy changes -> Rollback changes
- NixOS keeps bootloader entries to recent configurations
  - Rollback to previous config is possible
- Can be automated (e.g. using [CI pipeline](https://gist.github.com/dadada/c9184fef6dc7b66c8e94ecf65783ce43) or cron-job).
- Know what is deployed and with which versions.

## agenix

- Problem: Everything that is part of a configuration is world-readable in `/nix/store`!
- *Solution 1*: Manually copy secrets to target hosts.
- *Solution 2*: [agenix](https://github.com/ryantm/agenix) uses `age` to encrypt secrets (e.g. password, keys) in the Nix store.
- Files are encrypted to host's SSH key.
- Path to decrypted files can be used in NixOS configuration.

## devshell

- [devshell](https://github.com/numtide/devshell)
- Convenient shell for deploying and managing configuration.
- Puts all required tools into `$PATH` (e.g. `agenix`).
- Define custom commands (e.g. `deploy warhead` to run `nixos-rebuild switch --flake .#warhead --use-remote-sudo --use-substitutes --target-host warhead`).

## Build Images

- [nixos-generators](https://github.com/nix-community/nixos-generators).
- Prepare installer image with
  - Admin account
  - sshd + Authorized keys
  - partitioning and installation script
- Build very small Docker and VM images.

## NixOS Tests

`./nixos-test.nix`

## Resources

- [nix.dev](https://nix.dev/): Getting things done with Nix
- [nixos-hardware](https://github.com/NixOS/nixos-hardware): Hardware-specific tweaks.
- [flake-utils](https://github.com/numtide/flake-utils): Utility functions for use in flakes.
